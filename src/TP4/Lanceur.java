package TP4;

public class Lanceur {

	public static void main(String[] args) {
		Runnable compte1 = new Fct1();
		Thread a = new Thread(compte1);
		a.setName("Caroline");
		a.setPriority(1); // prio faible
		
		Thread b = new Thread(compte1);
		b.setName("Josiane");
		b.setPriority(5); // prio forte
		
		a.start();
		b.start();
	}

}
