package TP6;

public class Lanceur {

	public static void main(String[] args) {
		Runnable job = new JobSylvieEtBruno();
		
		Thread a = new Thread(job);
		a.setName("Sylvie");
		
		Thread b = new Thread(job);
		b.setName("Bruno");
		
		a.start();
		b.start();	
	}

}
