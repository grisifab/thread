package TP5;

public class Lanceur {

	public static void main(String[] args) {
		Runnable compte1 = new Fct1();
		
		Thread a = new Thread(compte1);
		a.setName("Caroline");
		
		Thread b = new Thread(compte1);
		b.setName("Josiane");
		
		Thread c = new Thread(compte1);
		c.setName("Cun�gonde");
		
		a.start();
		b.start();
		c.start();
		
		 try 
		 { 
		     a.join(); 
		     b.join(); 
		     c.join(); 
		 } 
		      catch(InterruptedException exc) {}
		 
		 System.out.println("Execution termin�e"); 
		
	}

}
